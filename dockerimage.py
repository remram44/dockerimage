import codecs
import hashlib
import json
import logging
import tarfile


logger = logging.getLogger('dockerimage')


class InvalidDockerArchive(ValueError):
    """The Docker archive you are trying to load is invalid.
    """


def hash_file(fileobj):
    hasher = hashlib.sha256()
    chunk = fileobj.read(4096)
    while len(chunk) == 4096:
        hasher.update(chunk)
        chunk = fileobj.read(4096)
    if chunk:
        hasher.update(chunk)
    return hasher.hexdigest()


class Archive(object):
    """An TAR bundle, used by ``docker save`` and ``docker load``.

    In it are:
      * layers, as individual TAR files + JSON, with some hex ID
      * images description JSONs, content-addressed, referencing layers by hash
      * a list ``repo:tag`` associated to an image description hash
      * a manifest, listing the image descriptions files, repotags, and layers
        contained in the archive
    """
    def __init__(self, **kwargs):
        utf8 = codecs.getreader('utf-8')

        # Open file from arguments
        if 'filename' in kwargs:
            self._file = tarfile.open(kwargs.pop('filename'), 'r:*')
        elif 'fileobj' in kwargs:
            self._file = tarfile.open(fileobj=kwargs.pop('fileobj'))
        else:
            raise TypeError("Requires one of 'filename' or 'fileobj'")
        if kwargs:
            raise TypeError("%r is an invalid keyword argument for this "
                            "function" % next(iter(kwargs)))

        # Load base files
        names = self._file.getnames()
        for name in ['manifest.json', 'repositories']:
            if name not in names:
                raise InvalidDockerArchive("Required file %r is missing from "
                                           "archive" % name)
        manifest = json.load(utf8(self._file.extractfile('manifest.json')))

        # Check for unknown or missing files
        expected_names = {'manifest.json', 'repositories'}
        for image in manifest:
            expected_names.add(image['Config'])
            expected_names.update(image['Layers'])
        for name in expected_names:
            if name not in names:
                raise InvalidDockerArchive("File %r is mentioned in manifest "
                                           "but not present in archive" % name)
        expected_names = [name.split('/', 1)[0] for name in expected_names]
        for name in names:
            if name.split('/', 1)[0] not in expected_names:
                raise InvalidDockerArchive("File %r is present in archive but "
                                           "not mentioned in manifest" % name)

        # Read in manifest
        repositories = {}
        image_layers = {}
        image_digests = {}
        image_tags = {}
        for config in manifest:
            layers = [e.split('/', 1)[0] for e in config['Layers']]
            for repotag in config['RepoTags']:
                repo, tag = repotag.rsplit(':', 1)
                repositories.setdefault(repo, {})[tag] = layers[-1]
                image_layers[config['Config']] = layers
                digest = config['Config'].split('.', 1)[0]
                image_digests[repotag] = digest
                image_tags.setdefault(digest, set()).add(repotag)
        logger.info("Loaded manifest: %d repositories, %d tags",
                    len(repositories), len(image_digests))

        self.layers = {}
        self.layers_by_digest = {}

        # Load the images
        self.images_by_digest = {}
        self.images = {}
        for image_json, layer_names in image_layers.items():
            image_digest = image_json.split('.', 1)[0]
            tags = image_tags[image_digest]
            if tags:
                tags = " (%s)" % ", ".join(tags)
            logger.info("Loading %s%s, %d layers...",
                        image_json, tags, len(layer_names))
            json_file = self._file.extractfile(image_json)
            hash = hash_file(json_file)
            if hash != image_digest:
                raise InvalidDockerArchive(
                    "Image config %r%s has invalid digest (%r)" % (
                        image_json, tags, hash
                    ))
            json_file.seek(0)
            image_config = json.load(utf8(json_file))
            if image_config['rootfs']['type'] != 'layers':
                raise InvalidDockerArchive("Invalid image config %r%s" % (
                    image_json, tags))
            layers = []
            for layer_name, layer_digest in zip(
                    layer_names, image_config['rootfs']['diff_ids']):
                # Load the layer
                if layer_name in self.layers:
                    logger.info("Reusing layer %s", layer_name)
                    layer = self.layers[layer_name]
                else:
                    logger.info("Loading layer %s...", layer_name)
                    layer_json = json.load(
                        utf8(self._file.extractfile('%s/json' % layer_name)))
                    parent = layer_json.get('parent')
                    if parent is not None:
                        parent = self.layers[parent]
                    layer = Layer(
                        name=layer_name,
                        tar=self._file.extractfile('%s/layer.tar' %
                                                   layer_name),
                        json=layer_json,
                        parent=parent,
                    )
                    logger.info("digest = %s", layer.digest)
                    self.layers[layer_name] = layer
                    self.layers_by_digest[layer.digest] = layer

                if layers:
                    if layer.parent is None:
                        raise InvalidDockerArchive(
                            "In image config %r%s %r is missing parent "
                            "reference (should be %r)" % (
                                image_json, tags, layer,
                                layers[-1]))
                    elif layer.parent is not layers[-1]:
                        raise InvalidDockerArchive(
                            "In image config %r%s %r parent reference "
                            "is not previous layer (%r != %r)" % (
                                image_json, tags, layer,
                                layer.parent, layers[-1]))
                else:
                    if layer.parent is not None:
                        raise InvalidDockerArchive(
                            "In image config %r%s %r is used first but "
                            "it has parent reference %r" % (
                                image_json, tags, layer,
                                layer.parent))

                layers.append(layer)

                # Check the hash
                if not layer_digest.startswith('sha256:'):
                    raise InvalidDockerArchive("Invalid layer reference %r" %
                                               layer_digest)
                layer_digest = layer_digest[7:]
                if layer_digest != layer.digest:
                    raise InvalidDockerArchive(
                        "In image config %r%s %r doesn't have the required "
                        "digest sha256:%s" % (image_json, tags,
                                              layer, layer_digest))

            # Construct the image
            image = Image(image_digest, image_config, layers)
            self.images_by_digest[image_digest] = image
            for repotag in image_tags[image_digest]:
                self.images[repotag] = image

        # Check consistency of 'repositories' file
        if repositories != json.load(
                utf8(self._file.extractfile('repositories'))):
            raise InvalidDockerArchive("'repositories' file doesn't match "
                                       "'manifest.json'")

    def close(self):
        self._file.close()

    def __repr__(self):
        images = {}
        for tag, img in self.images.items():
            images.setdefault(img.digest, (img, set()))[1].add(tag)
        return "<%s layers=[%s] images=[%s]>" % (
            self.__class__.__name__,
            ", ".join('%s (sha256:%s)' % (l.name, l.digest)
                      for l in self.layers.values()),
            ", ".join('digest=%s top_layer=sha256:%s tags=[%s]' % (
                          h, i.layers[-1].digest, ", ".join(t)
                      )
                      for h, (i, t) in images.items()),
        )

    __str__ = __repr__
    __unicode__ = __repr__


class Layer(object):
    def __init__(self, name, tar, json, parent=None, hash=None):
        self.name = name
        self._file = tar
        self._json = json
        if hash is None:
            hash = hash_file(self._file)
        self.digest = hash
        self.parent = parent

        if self.parent is not None:
            if 'parent' not in self._json:
                raise ValueError("Layer's parent doesn't appear in JSON")
            elif self._json.get('parent') != self.parent.name:
                raise ValueError("Layer's parent doesn't match name in JSON")
        else:
            if 'parent' in self._json:
                raise ValueError("Layer doesn't have a parent but one appears "
                                 "in JSON")

    def __repr__(self):
        return "<%s %s (sha256:%s)>" % (self.__class__.__name__,
                                        self.name, self.digest)

    __str__ = __repr__
    __unicode__ = __repr__


class Image(object):
    def __init__(self, digest, config, layers):
        self.digest = digest
        self._json = config
        self.layers = layers

    def __repr__(self):
        return "<%s digest=%s layers=[%s]>" % (
            self.__class__.__name__,
            self.digest,
            ", ".join('%s (sha256:%s)' % (l.name, l.digest)
                      for l in self.layers),
        )

    __str__ = __repr__
    __unicode__ = __repr__
