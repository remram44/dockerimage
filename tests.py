import logging
import os
import subprocess
import tempfile
import unittest

import dockerimage


class TestArchive(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls._dir = os.path.join(tempfile.gettempdir(), 'dockerimage_test')
        cls._archive = os.path.join(cls._dir, 'archive.tar')
        cls._images = ['remram/test-busybox:latest',
                       'remram/testimages:01', 'remram/testimages:02',
                       'remram/testimages:03', 'remram/testimages:04']
        if os.path.exists(cls._archive):
            return
        if not os.path.exists(cls._dir):
            os.mkdir(cls._dir)
        for image in cls._images:
            subprocess.check_call(['docker', 'pull', image])
        subprocess.check_call(['docker', 'save',
                               '-o', cls._archive] + cls._images)

    def test_open(self):
        archive = dockerimage.Archive(filename=self._archive)
        try:
            self.assertEqual(set(archive.images.keys()), set(self._images))
            all_layers = archive.images['remram/testimages:04'].layers
            all_layers = [l.digest for l in all_layers]
            self.assertEqual(len(all_layers), 5)
            seen_layers = set()
            for image in self._images:
                layers = archive.images[image].layers
                for layer in layers:
                    seen_layers.add(layer.name)
                layers = [l.digest for l in layers]
                self.assertEqual(
                    layers,
                    all_layers[:len(layers)],
                )
            self.assertEqual(seen_layers, set(archive.layers))
        finally:
            archive.close()


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
