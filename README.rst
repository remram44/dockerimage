dockerimage
===========

This is a library for manipulating Docker images directly.

It will allow you to inspect, edit, or assemble Docker images in ``docker-save(1)`` format, without the need to talk to Docker at all.

Example
-------

..  code-block:: python

    import dockerimage

    archive = dockerimage.Archive(filename='ubuntu.tar.gz')
    print(archive.images.keys()
    # ['ubuntu:16.04', 'ubuntu:17.10']
    print(archive.images['ubuntu:16.04'].layers)
